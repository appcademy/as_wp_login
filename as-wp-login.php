<?php
/*
Plugin Name: Login con credenziali Area Adottanti
Version:     0.1.0
Author:      Matteo Mazzoni
Description: Plugin per unificare l'accesso a Wordpress utilizzando le credenziali dell'Area Adottanti
License:     GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// CONFIG AUTOUPDATE MECHANISM
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'http://bitbucket.org/appcademy/AS_WP_login/row/master/metadata.json',
    __FILE__
);

add_filter( 'authenticate', 'agatasmeralda_auth', 10, 3 );
function agatasmeralda_auth( $user, $username, $password ){
    $AS_DB_HOST='localhost';
    $AS_DB_USER='garibaldipublic';
    $AS_DB_PASS='ETHso2$lwJKiw82';
    $AS_DB_NAME='agatas5_garibaldi';
    $AS_DB_CHARSET='latin1';

    if ( is_a( $user, 'WP_User' ) ){
        return $user;
   }
   // Make sure a username and password are present for us to work with
    if($username == '' || $password == '') return;
    $logincorrect=2;

    /**  duplicare meccanismo login di Area Adottanti
     **  settare la variabile $logincorrect coi seguenti valori:
     **  0 -> Credenziali errate (utente presente ma password errata)
     **  1 -> Accesso correto
     **  2 -> Utente non presente nel db Area Adottanti (si tenta l'autenticazione standard di wordpress)
     **/

   $db = new mysqli($AS_DB_HOST, $AS_DB_USER, $AS_DB_PASS, $AS_DB_NAME);
   $db->set_charset($AS_DB_CHARSET);
   $result = $db->query("SELECT name, surname,email,password FROM sponsors WHERE email='$username'");
   $dati=null;
   if($result && $result->num_rows > 0) {
            // Estrae la password
            $dati = $result->fetch_assoc();
            if($dati['password'] == $password) {
                // ACCESSO OK
                $logincorrect=1;
            } else {
                // PASSWORD ERRATA
                $logincorrect=0;
            }
   } else {
    // NON ESISTE UTENTE o ERRORE DB
    $logincorrect=2;
   }

    if ($logincorrect == 0){
        // caso negativo
        $user = new WP_Error( 'denied', __("ERRORE: Password errata") );
    } 
    else if ($logincorrect == 1){
        // caso positivo
         $userobj = new WP_User();
         $user = $userobj->get_data_by( 'email', $dati['email'] ); // Does not return a WP_User object 🙁
         $user = new WP_User($user->ID); // Attempt to load up the user with that ID

         if( $user->ID == 0 ) {
             // The user does not currently exist in the WordPress user table.
             // You have arrived at a fork in the road, choose your destiny wisely

             // If you do not want to add new users to WordPress if they do not
             // already exist uncomment the following line and remove the user creation code
             // $user = new WP_Error( 'denied', __("ERROR: Not a valid user for this system") );

             // Setup the minimum required user information for this example
              $userdata = array( 'user_email' => $dati['email'],
                                 'user_login' => $dati['name'].'.'.$dati['surname'],
                                'first_name' => $dati['name'],
                                'last_name' => $dati['surname']
                                );
             $new_user_id = wp_insert_user( $userdata ); // A new user has been created

             // Load the new user info
             $user = new WP_User ($new_user_id);
         }
    }

    // e.g. $login correct = 2
    // Comment this line if you wish to fall back on WordPress authentication
    // Useful for times when the external service is offline

    // remove_action('authenticate', 'wp_authenticate_username_password', 20);

    return $user;
}


